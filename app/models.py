from django.db import models


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()
    mobile = models.CharField(unique=True, max_length=15)

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserMobileActivation(models.Model):
    code = models.CharField(max_length=50)
    create_date = models.DateTimeField()
    is_used = models.BooleanField()
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_mobile_activation'
