from flask import Flask
from django.apps import apps
from django.conf import settings


apps.populate(settings.INSTALLED_APPS)

from app.models import AuthUser

app = Flask(__name__)

@app.route("/")
def index():
    return str(AuthUser.objects.count())

if __name__=='__main__':
    app.run(debug=True)
