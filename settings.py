import dj_database_url

DATABASES = { 'default': dj_database_url.config(default='postgres://')}

INSTALLED_APPS = ( 'app', )

SECRET_KEY = 'REPLACE_ME'
